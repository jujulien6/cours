# Définir les volumes

Un conteneur est éphémère, par conséquent, il ne doit pas créer de données dans son systèmes de fichier. Le volume apparaît donc comme solution pour sauvegarder/garder nos fichiers.

Le volume :
- est extérieur au conteneur
- a un cycle de vie indépendant des conteneurs
- permet la persistance des données
- a la possibilité de partager entre conteneur d'un même pods.

Il existe plusieurs types de volumes : EmptyDir, HostPath, etc.

## EmptyDir

Il est lié au cycle de vie d'un pod.

On utilisera un fichier inspect.yml, on rajoutera une partie volume EmptyDir.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: inspect
spec:
  containers:
  - name: inspect
    image: alpine:3.10
    command: ["sleep","3600"]
    volumeMounts:
    - name: datas
      mountPath: /datas
  volumes:
  - name: datas
    emptyDir: {}
```

On applique la configuration de notre fichier **inspect.yml**. On vérifie que les pods sont bien crée.  Enfin, on lance un shell en mode interactif :

```bash
kubectl apply -f inspect.yml
kubectl get pods
kubectl exec -it inspect sh
```
On vérifie ensuite dans notre conteneur que le point de montage **datas** soit bien crée :

```bash
df
```
On crée un fichier dans notre point de montage, tant que le pod est existant, le fichier apparaîtra, dès que le pod est supprimé, le fichier aussi.

## HostPath

Il permet de partager un répertoire de notre machine localhost vers notre pods.
On utilise le fichier inspect.yml.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: inspect
spec:
  containers:
  - name: inspect
    image: alpine:3.10
    command: ["sleep","3600"]
    volumeMounts:
    - name: docker
      mountPath: /var/lib/docker
  volumes:
  - name: datas
    HostPath:
      path: /var/lib/docker
```

On applique la configuration de notre fichier **inspect.yml**. On vérifie que les pods sont bien crée.  Enfin, on lance un shell en mode interactif :

```bash
kubectl apply -f inspect.yml
kubectl get pods
kubectl exec -it inspect sh
```

On vérifie ensuite dans notre conteneur que le point de montage **datas** soit bien crée :

```bash
df
```

## Persistant Volume (PV)

Il existe plusieurs manière de créer un créer un Persistant Volume :

- Stockage provisionné (NAS)
- Statique (On-Premise)
- Dynamique (Cloud)

Une fois notre Persistant Volume crée, on utilisera un `Persistent Volume Claim` qui nous servira pour :

- Faire une demande de stockage
- Spécifier des contraintes (taille, type, etc.)
- Consommer un PV existant