# S'initier au namespaces

Un **namespace** est utilisé pour segmenter/partitionner votre cluster pour avoir une vue logique et clair de celui-ci. Il va nous permettre d'isoler certaines ressources du cluster comme des pods, services, déploiements. Par exemple, on peut séparer par clients, secteur, départements, projets de votre entreprise.

Vous pouvez créer vos propres namespace :

```bash
kubectl create namespace production
```
Ou à partir d'un fichier de spécification yaml :

```yaml
apiversion: v1
kind: Namespace
metadata:
  name: production
```
```bash
kubectl apply -f namespace.yml
```

Pour obtenir la liste des namespace crée sur votre cluster Kubernetes :

```bash
kubectl get namespace
```
A savoir que Kubernetes s'installe avec deux cluster par défaut que sont `kube-system` et `default`.

Le namespace est asigné dans les métadonnées de votre fichier de spécification, il est définit de la manière suivante :

```yaml
apiversion: v1
kind: Pod
metadata:
  name: web
  namespace: production
spec:
  containers:
  - name: www
    image: nginx:1.17
```
Le namespace peut aussi être assigné dans un context d'utilisation :

```bash
kubectl config set-context minikube --namespace=production
```
Dans cet exemple, dès qu'on utilise le `context minikube`, nos pods crée seront automatiquement affectés au `namespace production`.