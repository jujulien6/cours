## Découvrir les services et la gestion du réseau

Le `service` est une ressource Kubernetes. Le service permet d'exposer notre application sur un port spécifique défini dans notre fichier de configuration.

### Gestion d'un service

Création d'un service à partir du fichier yaml

```bash
kubectl apply -f SERVICE_NAME.yml
```

Description d'un service

```bash
kubectl describe service SERVICE_NAME
```

Suppression d'un service

```bash
kubectl delete SERVICE_NAME
```

## Créer un service de type clusterIP

Le service ClusterIP permet de créer un réseau accessible depuis l'extérieur de notre cluster Kubernetes. On va donc avoir une résolution IP au niveau du nom du service pour qu'il soit accessible.

On aura besoin de deux fichiers.
Le fichier inspect.yml.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: inspect
spec:
  containers:
  - name: inspect
    image: alpine:3.10
    command: ["sleep","3600"]
```

Le fichier web.yml, dedans on ajoutera un label web qui va nous servir au niveau du service pour sélectionner ce pod.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: web
  labels:
    app: web
spec:
  containers:
  - name: nginx
    image: nginx:1.17-alpine
```

Puis, on crée un fichier service.yml avec comme port 8000 et comme port de destination 80.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: web
spec:
  selector:
    app: web
  type: ClusterIP
  ports:
    - port: 8000
      targetPort: 80
```

On lance notre apply.

```bash
kubectl apply -f .
```
Pour vérifier la disponibilité de nos ressources :

```bash
kubectl get pods, svc
```

Maintenant, testons ce que nous avons fait, en exécutant un shell de notre pod **inspect**.

```bash
kubectl exec -it inspect sh
```

On installe curl dans notre conteneur **inspect**. Au préalable, on récupère l'adresse IP de notre serveur web nginx.

```bash
kubectl describe nginx
```
Puis, toujours sur notre premier conteneur, on utilise la commande `curl` pour afficher le contenu du serveur web nginx.

```bash
curl <adresse_ip_serveur_nginx>:80
```

## Créer un service de type NodeIP

Le service `NodeIP` permet lui d'exposer à l'extérieur du cluster les pods associés a ce service. A l'inverse , le service `ClusterIP` qui lui expose les pods à l'intérieur du cluster Kubernetes.

On va réutiliser les fichiers **inspect.yml** et **web.yml**.

Le fichier inspect.yml.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: inspect
spec:
  containers:
  - name: inspect
    image: alpine:3.10
    command: ["sleep","3600"]
```

Le fichier web.yml.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: web
  labels:
    app: web
spec:
  containers:
  - name: nginx
    image: nginx:1.17-alpine
```

On réutilise le fichier service.yml mais on change le type de service en **nodePort**. On change le port en 8001 car 8000 déjà utilisé. On rajoute l'objet `nodePort` qui est le port du nœud exposé à l'extérieur du cluster Kubernetes.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: webnp
spec:
  selector:
    app: web
  type: NodePort
  ports:
    - port: 8001
      targetPort: 80
      nodePort: 30000
```

On fait notre apply.

```bash
kubectl apply -f .
```

On vérifie la disponibilité de nos ressources : 
```bash
kubectl get pods,svc
```

On lance notre pod inspect en mode interactif.

```bash
kubectl exec -it inspect sh
```
On installe curl sur notre conteneur. Au préalable, on récupère l'adresse IP de notre serveur web nginx avec `kubectl describe nginx`. Puis, on exécute simultanément :

 ```bash
apt update
apt install curl
curl <adresse_ip_serveur_nginx>:8001
```

Une fois que le contenu du service Nginx est bien affiché au niveau de notre cluster Kubernetes, on testera sur une autre machine local.

 ```bash
curl <adresse_ip_serveur_nginx>:30000
```