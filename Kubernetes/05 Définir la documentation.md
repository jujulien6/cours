## Prendre en main les outils de documentation

### Ressources

Vous pouvez retrouver la documentation en ligne de kubernetes via votre navigateur ou encore sur linux utiliser la commande :

```bash
kubectl --help
```

La commande **kubectl api-ressources** liste les ressources accessible par l'API. La sortie standard affichera :
- nom
- shortnames (raccourci)
- apigroup
- namespace sur lequel elle s'exécute
- kind (pod, service, node, etc.)

La commande **explain** permet d'avoir la documentation des ressources d'une sous commandes.
Ex : kubectl explain pod.spec.affinity.podAffinity
Avec cette commande, vous allez avoir le manuel pod, donc le KIND : Pod avec les versions etc. Ensuite, si vous rajoutez spec, vous aurez la partie spécification du pod. Si vous rajoutez affinity, vous aurez la partie affinité de la clé spec, et ensuite de pod.

### Mode Dry-Run

Ensuite, le mode **Dry-Run** qui permet de simuler une commande sans l'exécuter. Permet d'obtenir le résultat sans modification au niveau du cluster.

```bash
kubectl run --image=nginx web1 --dry-run
```

### Options de sortie

Avec l'option **-o yaml** ou **-o json**, vous allez pouvoir avoir un affichage en sortie de l'exécution d'une commande kubectl, cela peut-être très utile lors d'une suppression de pod ou encore lors d'un dry-run.

```bash
kubectl run --generator=run-pod/v1 --image=nginx web1 --dry-run -o yaml
```

### Format JSON

La commande **-o wide** nous permet de récupérer la sortie standard sur une information d'une ressource recherché à l'aide de **kubectl**. Vous pouvez très bien filtrer la sortie de sorte à n'obtenir que l'élément qui vous intéresse. Par exemple le premier item ou encore le nom de notre pods. Ex :

```bash
kubectl get pods -o jsonpath='{.item[0]}'
kubectl get pods -o jsonpath='{.item[0].metadata.name}'
```

## Installer la complétion sous Linux

Sur notre CentOS, on va se connecter en tant que root. On va installer la partie bash complétion.

```bash
yum install bash- completion -y
```

Ensuite, on exécute la commande pour faire la complétion de kubectl tout en redirigeant la commande vers le fichier qui contient les complétions :

```bash
kubectl completion bash > /etc/bash_completion.d/
```

On se déconnecte en tant que root. On se connecte à notre user CentOs, et on rajoute la complétions pour l'utilisateur CentOS.


```bash
echo 'source <(kubectl completion bash)' >> /bashrc
```

Ensuite, vous pourrez utiliser la commande kubectl **tab**, et utiliser ce raccourci pour aller plus vite.



