# Découvrir les déploiements

La ressource **deployment** vous permet de créer, gérer, mettre à l'échelle et maintenir la disponibilité de vos pods.

Le **deployment** gère des **ReplicaSet**. Les **replicaSet** sont des pods de même spécification, ils permettent de faire du load-balancing, très utile par exemple dans l'utilisation d'un serveur web.

Le **deployment** gère aussi les mises à jour. Lorsqu'on fait un **update** ou un **rollback** de notre application, celle-ci sera toujours disponible grâce à nos **replicaSet**.

Utilisons le fichier ci-dessous, web-deployment.yaml :

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web-deployment
  labels:
    app: web
spec:
  replicas: 3
  selector:
    matchLabels:
      app: web
  template:
    metadata:
      labels:
        app: web
    spec:
      containers:
      - name: nginx
        image: nginx:1.17
        ports:
        - containerPort: 80
```

Dans ce cas ci, on veut 3 pods en replicaSet.

On fait notre apply :

```bash
kubectl apply -f web-deployment.yml
```

On vérifie notre déploiement :

```bash
kubectl get deployments
```

On vérifie nos réplicas :

```bash
kubectl get replicasets
```

On vérifie que nos pods existent :

```bash
kubectl get pods
```

# Mettre à jour un déploiement

On va voir à travers ce module la notion de `rolling update` ou `rollback`.

Pour cela, on utilise le fichier **web.yaml** ci-dessous pour notre déploiement :

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web
spec:
  replicas: 2
  selector:
    matchLabels:
      app: web
  template:
    metadata:
      labels:
        app: web
    spec:
      containers:
      - name: nginx
        image: nginx:1.16-alpine
        ports:
        - containerPort: 80
```

On le déploie :

```bash
kubectl apply -f web.yml
```

On souhaite obtenir un peu plus d'informations sur notre déploiement :

```bash
kubectl get deploy -o wide
```

Mais aussi nos pods :

```bash
kubectl get pods -o wide
```

Maintenant, on met à jour la version de l'image nginx en 1.17 dans notre fichier de configuration **web.yaml**. Après avoir fait un **apply** de notre configuration, on va vouloir vérifier que notre image nginx est bien 1.17.

```bash
kubectl describe web
```

Imaginons maintenant que la version 1.17 de nginx comporte quelques failles de sécurité, nous souhaitons revenir à la version précédente. Pour cela, on utilise la commande :

```bash
kubectl rollout undo deploy/web
```

On peut à nouveau vérifier l'état de notre pod ou notre déploiement.
Une autre façon de mettre à jour :

```bash
kubectl set image deploy/web nginx=1.15-alpine
```

On mettra à jour notre image de notre déploiement web en nginx1.15-alpine par exemple.