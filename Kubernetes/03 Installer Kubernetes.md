# Aborder les prérequis pour installer On-Premise

Pour cette partie-là, nous allons configurer un cluster Kubernetes avec 3 nœuds (1 master, 2 workers).
Dans un premier temps, nous allons vérifier les prérequis pour installer **kubeadm**.
- Vérifier la version de votre machine, ici CentOs en 7.7
- Vérifier que Docker soit installé.
- Ajouter l'adresse Ip des machines de votre cluster au fichier de configuration **etc/hosts/**.
- Installer les prérequis firewall sur les différentes machines.

Se référer à la [documentation](https://kubernetes.io/fr/docs/setup/production-environment/tools/kubeadm/).

# Configurer le cluster On-Premise

Dans cette partie-là, nous installerons un cluster à master unique. Dans un premier temps, on initialisera notre machine `master`. Pour cela, on doit :

1. Initialiser Kubeadm. Kubeadm vérifie que la machine vérifie que votre machine est prête à utiliser Kubernetes. Elle installe aussi tous les composants du **control plane components**.

```shell
kubeadmin init
```
2. Pour que vous puissiez utiliser kubectl en tant qu'utilisateur `non root`, vous pouvez exécuter la suite de commandes :

```shell
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

3. Choisir un add-on-réseau, par exemple `calico` pour que nos pods puissent communiquer. Pour installer calico :

```shell
kubectl apply -f https://docs.projectcalico.org/v3.8/manifests/calico.yaml
```
Pour vérifier que calico soit bien installé :

```shell
kubectl apply -f https://docs.projectcalico.org/v3.8/manifests/calico.yaml
```

4. Par défaut, aucun pod n'est déployé sur votre noeud **master**, en revanche, vous pouvez changer cela (optionnel et non conseillé) :

```shell
kubectl taint nodes --all node-role.kubernetes.io/master-
```

5. Une fois tous les prérequis fait, il est maintenant tant de faire rejoindre vos différents nœuds.

```shell
kubeadm join
```

6. La dernière étape conste à vérifier l'installation de kubeadm sur vos différents nœuds en exécutant la commande suivante qui affichera la liste des nœuds de votre cluster. On rappelle qu'il existe 3 nœuds (1 master et 2 worker).

```shell
kubectl get nodes
```

## Comprendre le contexte d'utilisation

Le **context** définit trois informations principales (cluster, namespace, utilisateur). Pour avoir la liste des context :

```bash
kubetl config get-contexts
```

Dedans on aura :
- current : context courant au niveau de la commande kubectl
- name : nom context
- cluster : nom cluster
- auth info : nom de l'utilisateur pour se connecter au context
- namespace : si il y a un namespace affiliée à ce context

Pour changer de context :

```bash
kubetl config use-contexts <nom_context>
```

Pour visualiser un context :

```bash
kubetl config view
```

Pour supprimer un context :

```bash
kubetl config delete-context <nom_context>
```