# Comprendre l'architecture Kubernetes

## Utiliser la terminologie Kubernetes

Kubernetes est un orchestrateur de conteneur. Un cluster Kubernetes est constitué de serveur(noeud). A partir de la version 1.14, on peut les déployer sur Linux ou Windows.

Il existe deux types de nœuds :
1. le **master**
2. les **workers**

### Le master

Le **control plane components** est en quelques sortes le cerveau de notre cluster Kubernetes, ce sont ces composants qui vont nous permettre de manager notre cluster Kubernetes.

#### ETCD

**ETCD** est le composant le plus important au sein de notre nœud master, tous les états des ressources Kubernetes sont stockés dans le composant ETCD. C'est un magasin de données clé-valeur distribuée qui contient toutes les informations du cluster Kubernetes.

#### Kube-apiserver

**Kube-apiserver** permet d'exposer l'api Kubernetes, tous ses composants communiquent avec l'apiserver, c'est en revanche la seule qui communique avec ETCD. 

#### Kube-scheduler

Le **Kube-scheduler** a pour rôle d'effectuer d'affecter des pods à des nœuds, c'est lui qui gère le mécanisme de création, suppression, mise à jour de nos pods.

#### Kube-controller-manager

Le **Kube-controller-manager** contient des contrôleurs qui sont chargés de vérifier l'état réel du cluster vers l'état souhaité. On retrouve des contrôleurs de types :
- nœud
- réplica
- service
- deployment
- namespace

### Les Workers

Un cluster Kubernetes est un ensemble de **workers**. Les workers hébergent les pods qui seront les composants de notre application.

Pour que les clients puissent accéder à un cluster Kubernetes, ils auront besoin de :
- fichier de configuration pour la connexion à notre cluster Kubernetes
- droits d'accès pour les utilisateurs sur le cluster Kubernetes

## Etudier les ressources du cluster

Il existe plusieurs catégories de ressources au sein d'un cluster Kubernetes.

### Gestion des applications

Un pod est crée à partir d'un **deployment**, le deployment permet de définir l'état souhaité d'un pod. Un pod contient un ou plusieurs conteneurs. Le **ReplicaSet** va nous permettre de définir le nombre de pods souhaités.

### Load balancing

Le **service** nous permet de contrôler l'accès aux pods à l'intérieur ou à l'extérieur du cluster.

### Configuration des applications

La ressource **ConfigMap** va nous permettre de gérer la configuration de notre application (mot de passe, etc.).

### Stockage 

Les **Persistant Volume (PV)** permettent de créer des volumes externes au cluster (stockage). La ressource **Volume claim** permet de faire une demande de PV suivant un certains nombre de paramètres.

### Configuration du cluster 

In a Kubernetes environment, metadata can be a crucial tool for organizing and understanding the way containers are orchestrated across your many services, machines, availability zones or (in the future) multiple clouds.

Les **metadatas** sont des éléments importants au sein d'un cluster Kubernetes. Ils permettent de comprendre comment sont organisés nos ressources (namespace, service, rôles, etc.).

## Définir le format des spécifications des ressources

Pour créer ou mettre à jour, nos ressources au niveau de notre cluster Kubernetes, il existe des fichiers de spécification. Ex :

```yaml
apiversion: v1
kind: Pod
metadata:
name: nginx
spec:
containers:
- name: www
image: nginx:1.12.2
```
- apiVersion: version de l'Api.
- kind: types de ressources crée.
- metadata: informations sur nos ressources.
- spec : spécifier comment gérer nos ressources

Les fichiers de définitions sont généralement écrits en **yaml**, ils peuvent être aussi en format **json**.