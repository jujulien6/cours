## Créer son premier pods

Ce que vous pouvez faire au préalable, c'est créer un dossier de travail. Sous linux :

```bash
mkdir <nom_dossier>
```

Puis, crée son pods avec le fichier yaml suivant :
```bash
nano mon_premier_pods.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: web
spec:
  containers:
  - name: nginx
    image: nginx:1.17-alpine
```

Pour créer notre pods, utiliser la commande :
```bash
kubectl apply -f mon_premier_pods.yaml
```

Pour vérifier que le pods a été créé, utiliser la commande :
```bash
kubectl get pods
```

La commande describe nous permet d'obtenir plus d'informations sur une ressource (nom, type d'objets, label selector, events) :

```bash
kubectl describe <nom_du_pod>
```

Pour tester, que votre pods soit bien accessible vers l'extérieur, vous pouvez utiliser la commande suivante et ensuite ouvrir un navigateur (firefox par exemple) :

```bash
kubectl port-forward <nom_du_pod> <port_pod>:<port_souhaité>
```

## Créer un pod de test

On crée notre pods avec le fichier yaml suivant que l'on nommera **inspect.yaml** :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: inspect
spec:
  containers:
  - name: inspect
    image: alpine:3.10
    command: ["sleep","3600"]
```

L'objectif étant de mettre en attente le pod afin de lancer un shell pour interroger le premier pod. On crée notre pod.

```bash
kubectl apply -f inspect.yaml
```

Pour la suite, on devra récupérer l'ip du premier pod.

```bash
kubectl describe <nom_du_pod>
```
Pour pouvoir interroger notre premier pod, on va exécuter notre pod en terminal interactif :

```bash
kubectl exec -it inspect sh
```

On pourra ici-même faire un ping sur l'ip du premier pod :
```bash
kubectl ping <ip_adr>
```

## Executer des commandes de manipulation d'un pod

Pour cela, on va créer un troisième pod, dans lequel on va vouloir ping le localhost.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: ping
spec:
  containers:
  - name: ping
    image: alpine:3.10
    command: ["ping","127.0.0.1"]
```
On fait notre **apply**.

```bash
kubectl apply -f ping.yaml
```

Ensuite, on s'attardera sur plusieurs commandes.

### Exec

Permet de créer un processus à l'intérieur du container par défaut dans le pod. Par exemple un shell :

```bash
kubectl exec -it ping sh
```
A l'intérieur, de celui-ci, si on fait :

```bash
ps -ef
```
Cela nous affiche tous les PID, par exemple le PID 1 ici est la commande ping localhost.

### Logs

Permet de se connecter sur le premier processus qui s'exécute dans le container par défaut du pod. Ici, le processus ping.

```bash
kubectl logs -f <nom_du_pod>
```

### Attach

On peut très bien s'attacher à un pod. Ici, le pod ping. On se connecte sur le premier processus du container ping. D'habitude le pod s'utilise par défaut en mode détaché.

```bash
kubectl attach <nom_du_pod>
```

### Describe

La commande describe nous permet d'obtenir plus d'informations sur une ressource (nom, type d'objets, label selector, events) :

```bash
kubectl describe <nom_du_pod>
```

## Créer un pod avec plusieurs conteneurs

Après avoir vu comment crée un pod avec un seul conteneur, nous allons maintenant voir comment crée un pod avec plusieurs conteneurs. Pour cela, nous allons crée un fichier **web2c.yml**.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: web2c
spec:
  containers:
  - name: nginx
    image: nginx:1.17-alpine
  - name: inspect
    image: alpine:3.10
    command: ["sleep","3600"]
```

Créons le pod :

```bash
kubectl apply -f web2c.yaml
```

On va maintenant vouloir utiliser un shell avec le pod crée précédemment. Il ne faudra pas oublier de préciser le conteneur parce que il y en a plusieurs maintenant.

```bash
kubectl exec -it web -c inspect shell
```

En installant curl dans notre conteneur, on voudra voir si on a accès à notre service web. On rappelle que lorsque on exécute plusieurs conteneurs dans un même pod, ils communiquent entre eux.

## Aborder le mécanisme de scheduling

### Kube-schedular

Le **kube-schedular** va permettre de sélectionner le nœud sur lequel le pod va s'exécuter. Les éléments qui permettent de sélectionner le nœud sont **label** ou **étiquette**. Un label est une paire clé = valeur liée à tout objet au sens Kubernetes, comme les services, les déploiement, et donc les nœuds.
Les labels sont :
- Créés lors du déploiement ou ultérieurement
- Modifiés à tout moment
- Attributs des objets
- Organiser les applications sur le cluster

Exemple de label : "release" : "stable", "release" : "test"

### Les sélecteurs

- Sélection d'objets à partir de labels
- Identifie un ensemble spécifique d'objets
- Equality-based : (Egalité, différence, ex: =, !=)
- Set-based (valeur dans un environnement ou pas, ex: IN, NOTIN, EXIST)

### NodeSelector

Permet de lancer un pod sur un nœud ayant un label spécifique.

### NodeAffinity

Permet de créer une affinités entre un pod et un nœud.

### PodAntiAffinity

Ordonnancer un pod par affinité négative avec les autres pods.

## Exécuter un pod avec une affinité de type nœud

On travaillera avec le fichier yaml suivant.
  
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: web2c
spec:
  containers:
  - name: nginx
    image: nginx:1.17-alpine
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: kubernetes.io/hostname
            operator: In
            values:
            - worker1
            - worker2
      preferedDurringSchedulingIgnoredDuringExecution:
      - weight: 1
        preference:
          matchExpressions:
          - key: disktype
            operator: In
            values:
            - ssd
```

On a repris les bases d'un pod pour nginx en ajoutant **affinity**. 
On peut avoir deux types d'affinity :
- **requiredDuringSchedulingIgnoredDuringExecution**
- **preferedDurringSchedulingIgnoredDuringExecution**

`RequiredDuringSchedulingIgnoredDuringExecution` exécute le pod uniquement sur une machine qui a comme nom worker1 et worker2.

`PreferedDurringSchedulingIgnoredDuringExecution` s'exécute sur un nœud qui a comme valeur de label **disktype = ssd**. On doit donc rajouter la valeur clé disktype=ssd sur l'un des deux workers, pour cela on utilisera la commande kubectl.

```bash
kubectl label node worker2 disktype=ssd
```

On vérifie avec la commande describe sur le noeud worker2.

```bash
kubectl describe worker2
```

On crée notre pod.

```bash
kubectl apply -f <nom_du_fichier>
```

On vérifie à l'aide de la commande kubectl describe.

Pour supprimer le label disktype précédemment crée :

```bash
kubectl label node worker2 disktype-
```
On vérifie encore à l'aide de kubectl describe. On s'aperçoit que le pod est toujours en exécution sur le worker2 alors que le label a été supprimé, cela se fait pendant la phase de scheduling mais pas d'exécution.

## Exécuter un pod avec une affinité de type pod

Le pod affinity offre la possibilité de créer des affinités entre pod pour pouvoir par exemple les exécuter sur le même nœud. Pour cela, on utilisera deux fichiers.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: web1
  labels:
    app: web
spec:
  containers:
  - name: nginx
    image: nginx:1.17-alpine
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: web2
  labels:
    app: web
spec:
  containers:
  - name: nginx
    image: nginx:1.17-alpine
```

On lance nos pods avec le nom du répertoire.

```bash
kubectl apply -f <nom_répertoire>
```
On vérifie avec avec kubectl get pods, mais en ajoutant **-o wide** pour obtenir plus d'informations sur la sortie standard.

```bash
kubectl get pods -o wide
```

Les deux pods ont étés déployés mais sur deux noeuds différents.
On modifie notre fichier web2.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: web2
  labels:
    app: web
spec:
  containers:
  - name: nginx
    image: nginx:1.17-alpine
  affinity:
    podAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
      - labelSelector:
          matchExpressions:
          - key: app
            operator: In
            values:
          - web
        topologyKey: kubernetes.io/hostname
```
On a rajouter l'affinité avec **podAffinity**. Et comme labelSelector **app** qui est dans la liste web. Puis, on lance nos deux pods.

```bash
kubectl apply -f <nom_répertoire>
```

On vérifie en demandant toujours plus d'informations avec **-o wide**, on remarque que les deux pods s'exécutent sur le même nœud.

```bash
kubectl get pods -o wide
```