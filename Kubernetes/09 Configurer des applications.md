# Définir les ConfigMap

C'est un objet de Kubernetes qui permet de gérer la configuration de notre application. Il permet de détacher l'application de sa configuration, donc pas de configuration dans le code de notre application, mais aussi de pouvoir adapter la portabilité de l'application, on pourra modifier notre fichier suivant l'environnement sur lequel on évolue.

## Comment crée un fichier configmap

### A partir d'un fichier de données

Création de notre configmap :

```bash
kubectl create configmap nginx-conf --from-file=nginx.conf
```
Vérification configmap:

```bash
kubectl get configmap nginx-conf -o yaml
```

### A partir de variables d'environnement

Création de notre configmap :

```bash
kubectl create cm config-env --from-env-file=./conf.env
```

Vérification configmap:

```bash
kubectl get cm config-env -o yaml
```

### A partir d'un répertoire

Création de notre configmap :

```bash
kubectl create cm config-dir --from-file=confDir
```

Vérification configmap:

```bash
kubectl get cm config-dir -o yaml
```

### A partir de valeurs littérales

Création de notre configmap :

```bash
kubectl create cm config-lit --from-literal=env=PROD --from-literal=log_level=DEBUG
```

Vérification configmap:

```bash
kubectl get cm config-lit -o yaml
```

# Utiliser un ConfigMap dans un pod

Dans cet exemple, nous utiliserons un fichier ConfigMap de type variables d'environnement.
On a un fichier conf.env qui contient deux variables :
- environnement = prod
- log_level = debug

```env
env=prod
log_level=debug
```

Et un fichier pour crée un pod qui va s'appeler web.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: web
spec:
  containers:
  - name: www
    image: nginx:1.17
    env:
    - name: LOG_LEVEL
      valueFrom:
        configMapKeyRef:
          name: config-env
          key: log_level
    - name: SITE
      valueFrom:
        configMapKeyRef:
          name: config-env
          key: env
```

Dans notre fichier web.yml, on voit qu'on utilise comme référence de configMap **config-env**, on va donc crée ce fichier la issue de notre fichier conf.env.

```bash
kubectl create configmap config-env --from-env-file=conf.env
```

On vérifie :

```bash
kubectl get configmap -o yaml
```

On fait notre apply :

```bash
kubectl apply -f web.yaml
```

On exécute un shell pour afficher nos variables d'environnement :

```bash
kubectl exec -it web sh
```

Pour obtenir les variables d'environnement, la sortie affichera LOG_LEVEL et ENV :

```bash
env
```

# Utiliser un configMap de type Volume

Dans cet exemple, on utilisera un configMap de type volume.

On crée un fichier inspect-conf.yml

```yaml
apiVersion: v1
kind: configMap
metadata:
  name: inspect-conf
data:
  inspect.conf: |
    memory 2g
    cpu 2
```

On crée notre ConfigMap :

```bash
kubectl apply -f inspect-conf.yml
```

On vérifie:

```bash
kubectl get ConfigMap
kubectl get cm -o yaml
```

Maintenant, on va crée un pod qui utilise ce configMap.
On crée un fichier inspect.yml, en créant un volume dont le nom est généré par le fichier inspect-conf


```yaml
apiVersion: v1
kind: Pod
metadata:
  name: inspect
spec:
  containers:
  - name: inspect
    image: alpine:3.10
    command: ["sleep","3600"]
    volumeMounts:
    - name: data
      mountPath: "etc/conf"
  volumes:
    - name: data
      configMap: 
        name: inspect-conf
```

On fait notre apply, puis on vérifie :

```bash
kubectl apply -f inspect.yml
kubectl get pods -o wide yaml
```

On va ouvrir un shell dans notre pods, et vérifier le volume de stockage précédemment monté :
```bash
kubectl exec -it inspect sh
cat /etc/conf/inspect.conf
```

