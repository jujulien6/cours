# Le concept

Inspecter l'application crée dans votre cluster Kubernetes à l'aide de commande Helm

# Ce que j'ai compris

L'objectif de ce cours va être d'inspecter l'application précédemment crée afin d'aller plus loin avec Helm.

Dans un premier temps, à l'aide de la commande:
```bash
kubectl get all -n metrics
```
Cette commande nous permet de connaître l'état de nos pods, de notre déploiement, de notre service mais aussi de notre "replicaset".
Un "replicaset" est le nombre de pods crée pour notre déploiement, en général pour un containeur on en crée trois, cela permet un niveau de disponibilité élevée.

Ensuite, on va aller plus loin dans l'inspection. Chaque service, pods, service a un nom unique.
En utilisant la commande, vous obtiendrez des informations plus précises sur ce que vous voulez avec une date indiquant les différents événements :
```bash
kubectl get logs <name_of_the_pods> -n metrics
```

L'objectif d'après va être de pouvoir accéder à notre application en dehors de notre cluster kubernetes. A l'aide de la commande suivante, vous aurez accès à l'application kube-state-metrics sur le port 8080.
```bash
kubectl port-forward svc/kube-state-metrics 80:80 -n metrics
```
Sur votre navigateur internet, http://localhost:8080.
Vous aurez accès à deux informations :
1. **metrics**  
   Les différents logs que peut comporter votre application
2. **healthz**  
   Si votre application marche bien (OK si c'est le cas)

# Explication à un enfant de 8 ans

Prenons l'exemple d'un jeu, dans un premier temps une fois que le jeux sera installé, vous aurez par exemple la possibilité via un didacticiel d'apprendre à vous déplacer, de sauter par exemple. Une fois ces étapes maitrisé, vous devrez aller plus loin dans la maitrise de votre gameplay, vous apprendrez ensuite le double saut, ou encore la boule de feu pour tuer un ennemi. Pour l'inspection c'est exactement la même chose, vous allez d'abord installer votre application puis inspecter de plus en plus une fois que vous les étapes précédentes sont maitrisées.