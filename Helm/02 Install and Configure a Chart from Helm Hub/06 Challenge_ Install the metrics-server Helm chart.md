# Challenge

Ce challenge a pour but de tester les connaissances précédemment acquise. Pour cela nous devrons :

1. Créer un nouveau namespace appeler **challenge**
2. Installer dessus la chart Helm **Bitnami-hosted Metrics Server**
3. Nommer notre release **challenge-metrics-server**
4. Autoriser l'API metrics avec le "flag" : *--set apiService.create=true*