L'objectif de ce tp est de répondre à ce [challenge](Linkedin-Learning\Kubernetes: Package Management with Helm\02 Install and Configure a Chart from Helm Hub\06 Challenge: Install the metrics-server Helm chart.md):

Sur notre cluster Kubernetes, nous créerons dans un premier temps notre namespace **challenge** :
```bash
kubectl create namespace challenge
```

Une fois notre namespace crée, on vérifie dans un premier temps que celui-ci a bien été crée:
```bash
kubectl get namespace
```

L'étape d'après est de trouver l'url sur le [Hub](https://artifacthub.io/) Helm de l'application metrics-server par l'éditeur bitnami. Une fois trouvées, nous allons devoir l'ajouter à notre liste de repository Helm.
```bash
helm repo add challenge-metrics-server https://charts.bitnami.com/bitnami
```
Pour vérifier quel soit bien installer:
```bash
helm repo list
```

L'étape d'après consiste à l'installer dans notre cluster Kubernetes. Il ne faudra pas oublier d'autoriser l'API metrics avec le "flag" : --set apiService.create=true
```bash
helm install challenge-metrics-server challenge-metrics-server/metrics-server --set apiService.create=true -n challenge 
```

Finalement, on vérifie que le déploiement, les pods, les services sont bien déployées.
```bash
kubectl get all -n challenge
```