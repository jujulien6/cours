# Le concept

Ce module nous permet de traiter tout les cas de monter ou retour en version.

# Ce que j'ai compris

Un des nombreux avantages que peut représenter Helm est qu'il dispose d'une fonctionnalité de rollback qui permet aux utilisateurs de revenir à une version précédente d'une application en cas de problème ou une montée de version dans le cadre d'une mise à jour de sécurité ou de fonctionnalité de la part de l'éditeur.

Avec la commande suivante, vous avez la possibilité de savoir quel est la version de votre chart Helm actuellement installé sur votre cluster Kubernetes :
```bash
helm ls -n metrics
```
Le **-n** vous permettra de préciser le namespace sur lesquels vous voulez obtenir des informations.

A l'aide de cette commande vous obtiendrez deux informations :
1. **Chart version**
   version de votre chart Helm
2. **App version**
   version de votre code qui est installé sur vos pods

Souvent, vous n'allez pas chercher à avoir la dernière version de la chart Helm installé sur votre cluster Kubernetes. En effet, celle-ci pourrait comporter des erreurs, des problèmes de sécurité ou encore entraîner des erreurs sur vos différents micro services.

Pour "upgrade" votre version:
```bash
helm upgrade <name_application> <chart_helm> --version <number_version> -n <namespace>
```
Dans notre cas : 
```bash
helm upgrade kube-state-metrics bitnami/kube-state-metrics --version <number_version> -n metrics
```
Après avoir soit monter, ou fait un retour en arrière de version. En utilisant la commande :
```bash
kubectl get all -n metrics
```
Vous vous apercevrez que un nouveau pod ainsi que un nouveau replicaset ont été crées, celle-ci seront utilisés dès maintenant par votre application, une fois ceux-ci installés, les précédent seront détruits.

# Explication à un enfant de 8 ans

Imagine que tu as une application dans ton téléphone qui s'appelle "Super Jeux". Tu l'as téléchargée dans le Google Store et tu l'utilises tous les jours pour jouer à tes jeux préférés. Un jour, l'éditeur de "Super Jeux" décide de sortir une nouvelle version de l'application avec de nouveaux jeux et de nouvelles fonctionnalités. Cela signifie qu'il y a une mise à jour disponible dans le Google Store.
Tu peux mettre à jour "Super Jeux" en allant dans le Google Store et en cliquant sur le bouton "Mise à jour". Cela téléchargera la nouvelle version de l'application et l'installera sur ton téléphone. Si tu as des problèmes avec la nouvelle version, tu peux effectuer un rollback et retourner à l'ancienne version en allant dans le Google Store et en cliquant sur le bouton "Retour en arrière". Cela téléchargera l'ancienne version de l'application et l'installera à la place de la nouvelle version.
Il y a des outils comme Helm qui permettent aux gens de faire la même chose avec des applications sur un cluster Kubernetes, qui est un groupe de serveurs qui exécutent des applications. Au lieu de cliquer sur des boutons dans le Google Store, ils utilisent des commandes dans le terminal pour mettre à jour ou effectuer un rollback de leurs applications. C'est un peu comme si tu avais un Google Store pour tous les serveurs de ton entreprise et que tu pouvais mettre à jour ou effectuer un rollback de toutes les applications en même temps.