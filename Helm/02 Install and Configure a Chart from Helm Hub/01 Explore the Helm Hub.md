# Le concept

Helm Hub fournit une liste d'applications qui peuvent être installés sur votre propre cluster Kubernetes.

# Ce que j'ai compris

- C'est un gestionnaire de paquets pour Kubernetes qui rend la découverte et l'utilisation de logiciels sur la plateforme plus facile.
- Il permet aux utilisateurs de trouver et de déployer facilement des applications pré-configurées sur leur cluster Kubernetes.
- Le hub Helm est un outil très utile pour les utilisateurs de Kubernetes qui cherchent à automatiser leur déploiement et leur gestion d'applications.
- Il peut être utilisé par les développeurs pour publier et partager leurs applications avec la communauté Kubernetes.

Lien vers le hub : https://artifacthub.io/

# Explication à un enfant de 8 ans

Le hub Helm, c'est un peu comme Google Play pour ton téléphone. C'est un endroit où tu peux trouver des applications et des jeux qui peuvent être installés sur ton téléphone. Sauf que le hub Helm, c'est pour les gros ordinateurs qui utilisent Kubernetes. Alors, au lieu de trouver des applications pour ton téléphone, tu peux trouver des applications pour ces gros ordinateurs. Et ces applications peuvent t'aider à faire plein de choses, comme envoyer des messages, faire des calculs, ou même jouer à des jeux !