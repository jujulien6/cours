# Le concept

Aller plus loin avec Helm et les commandes.

# Ce que j'ai compris

Une fois l'inspection faites, le but de ce module est d'aller plus loin avec les commandes Helm.
Dans un premier temps, on va répertorier l'ensemble des commandes helm.
```bash
helm show help
```
Puis, on va aller plus loin. D'abord on va analyser notre chart kube-state-metrics.
```bash
helm show chart bitnami/kube-state-metrics
```
Celle ci regroupera plusieurs informations comme :
- la version de l'application
- la/les source(s)
- l'éditeur
- mots-clefs
- description
On peut les retrouver sur Helm Hub

Ensuite, on analysera les valeurs qui ont étés attribués à notre chart Helm, pour cela on peut utiliser la commande :
```bash
helm show values bitnami/kube-state-metrics
```
Mais pour que ce soit plus simple à décrypter, on redirigera la sortie vers un fichier yaml.
```bash
helm show values bitnami/kube-state-metrics > values.yaml
```
On peut retrouver par exemple dans ce fichier :
- le nombre de replicaset
- la port exposé 

A noter que par défaut, ce fichier est préremplies par l'éditeur, mais vous pouvez très bien l'adapter à votre besoin. Par exemple, nombre de replicaset = 3 pour un plus haut niveau de disponibilité.

On peut aussi obtenir le README associé à notre Helm chart.
```bash
helm show readme bitnami/kube-state-metrics
```

Ou encore toute les informations associées à notre chart.
```bash
helm show all bitnami/kube-state-metrics
```

# Explication à un enfant de 8 ans

Imagine que tu as un grand livre avec des instructions pour jouer à un jeu vidéo. Tu peux lire toutes ces instructions pour savoir comment jouer, comment personnaliser ton personnage, et comment utiliser toutes les touches de ton controller. C'est un peu comme ça que fonctionnent les commandes Helm.
Les commandes Helm permettent de gérer des programmes qui sont un peu comme des jeux vidéo, mais pour nos ordinateurs. Ces programmes s'appellent des "charts". Avec les commandes Helm, on peut regarder les instructions qui nous expliquent comment utiliser ces programmes, comment les personnaliser, et comment les faire fonctionner sur nos ordinateurs.
Par exemple, avec la commande helm show chart, on peut regarder les informations sur un chart qui est déjà installé sur notre ordinateur. On peut voir qui l'a créé, à quoi il sert, et comment on peut l'utiliser. Avec la commande helm show values, on peut regarder les options de personnalisation que l'on peut utiliser pour notre chart. Et avec la commande helm show readme, on peut lire les instructions qui nous expliquent comment utiliser notre chart au mieux.
En utilisant toutes ces commandes, on peut mieux comprendre comment nos programmes fonctionnent et comment les utiliser de manière efficace. C'est un peu comme si on avait un livre d'instructions pour chaque jeu vidéo que l'on joue !