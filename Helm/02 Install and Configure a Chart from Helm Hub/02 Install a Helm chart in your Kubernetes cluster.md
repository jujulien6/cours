# Le concept

Comment installer une chart Helm à l'aide Helm Hub.

# Ce que j'ai compris

Dans cet exemple, nous installerons kube-state-metrics à l'aide du Hub Helm.
Avant toute chose, le package kube-state-metrics est une applications nous permettant de :
- Collecter des datas sur notre cluster Kubernetes:
  - Sur nos nœuds
  - Sur nos pods
  - Sur notre déploiement
  - Sur notre "ConfigMaps"

Ensuite, rendons nous sur Helm Hub, et cherchons kube-state-metrics dans la barre de recherche. Il existe plusieurs versions de celle-ci, faites bien attention aux prérequis et si la chart a été fourni par l'éditeur.

Sur notre cluster Kubernetes:

Installation des charts Bitnami puis update de celle-ci:
```bash
helm repo add my-repo https://charts.bitnami.com/bitnami
helm repo update
```
Vérification des repository Helm installé:
```bash
helm repose list
```
Création d'un namespace pour l'installation de kube-state-metrics
```bash
kubectl create ns metrics
```

Installation de kube-state-metrics sur notre cluster Kube dans le namespace crée précédemment :
```bash
helm install kube-state-metrics bitnami/kube-state-metrics -n metrics
```
Vérification de l'installation de notre kube-state-metrics:
```bash
helm ls -n metrics
```

Helm Hub: https://artifacthub.io/

# Explication à un enfant de 8 ans

Installer a Helm Chart dans notre cluster Kubernetes, revient à cliquer sur le bouton installer lorsque vous choisissez une application dans le Google Play.Le hub Helm, c'est comme un magasin d'applications pour les gros ordinateurs qui utilisent Kubernetes. Tu peux aller sur ce magasin et trouver plein d'applications différentes qui peuvent t'aider à faire plein de choses. Pour trouver une application que tu veux, tu dois d'abord chercher sur le magasin et télécharger l'application sur ton ordinateur. Ensuite, tu peux l'installer sur ton gros ordinateur en utilisant un programme appelé Helm. Et une fois que tu as installé l'application, tu peux l'utiliser pour faire plein de choses ! C'est un peu comme quand tu installes des jeux sur ton téléphone, sauf que c'est pour les gros ordinateurs.