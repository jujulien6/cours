# Le concept

Installer Helm avec Windows

# Ce que j'ai compris

Avec le package chocolaty : choco install kubernetes-helm  
Avec la source "binary release"

Documentation : https://helm.sh/docs/intro/install/

# Expliquer à un enfant de 8 ans

Pour installer Helm sur un ordinateur Windows, tu dois suivre ces étapes :


1. Télécharge l'application Helm sur Internet. C'est comme un jeu ou une vidéo que tu peux télécharger sur ton ordinateur.


2. Ouvre l'application Helm une fois qu'elle est téléchargée. C'est comme si tu voulais jouer à un nouveau jeu sur ton ordinateur.


3. Suis les instructions à l'écran pour installer Helm sur ton ordinateur. C'est comme si tu devais suivre les instructions d'un puzzle pour savoir comment le monter.


4. Une fois que Helm est installé, tu peux l'utiliser pour ajouter de nouvelles fonctionnalités à ton ordinateur. C'est comme si tu pouvais ajouter de nouveaux jouets à ta boîte à jouets.


5. Pour utiliser Helm, tu devras peut-être ouvrir une fenêtre de commande sur ton ordinateur. C'est comme si tu devais parler à ton ordinateur en tapant des mots sur le clavier.


6. Tu peux maintenant utiliser Helm pour ajouter de nouvelles choses à ton ordinateur. C'est comme si tu pouvais ajouter de nouveaux jouets à ta boîte à jouets chaque fois que tu en as envie !