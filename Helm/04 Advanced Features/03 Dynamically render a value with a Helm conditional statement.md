# Le concept

Dans ce module, nous allons voir comment utiliser les conditions **if/else** pour rendre nos valeurs dynamiques.

# Ce que j'ai compris

Le moteur de template Helm a la possibilité d'utiliser les boucles **if/else** suivant si une condition soit respecté ou non, ces conditions fourniront la possibilité de rendre nos valeurs de manière dynamique.

Reprenons le fichier **configMap.yaml** que nous avons vu dans un précédent module. De manière générale, les développeurs s'occuperont des tests avant de lancer l'application en production. Pour cela, on va configurer le port 8080 et avec le paramètre **allow-Testing** a "true" pour pouvoir tester notre application en environnement de test et non en production.

Le fichier **configMap.yaml** avant l'utilisation des boucles :

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: first-chart-configmap-{{.Chart.Version}}
data:
  port: "8080"
  allowTesting: "true"
```

Dans un fichier yaml, les conditions s'écrivent de la manière suivante :

```yaml
{{if}}
{{else}}
{{end}}
```

Nous allons maintenant vouloir définir notre variable **allowTesting** a "true" si c'est un environnement de test, et "false" sinon.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: first-chart-configmap-{{.Chart.Version}}
data:
  port: "8080"
  {{ if eq .Values.env "staging"}}
  allowTesting: "true"
  {{else}}
  allowTesting: "false"
  {{end}}
```


Ensuite, dans notre fichier **values.yaml**, on initialisera la variables **env : production**. Normalement, lorsqu'on utilisera notre chart Helm, **allow-Testing** sera initialisé à "false". Allons tester ceci. 

Le fichier **values.yaml** :

```yaml
# Default values for first-chart.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

env: staging

replicaCount: 1

image:
  repository: nginx
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: ""

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: ClusterIP
  port: 80

ingress:
  enabled: false
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: chart-example.local
      paths: []
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

nodeSelector: {}

tolerations: []

affinity: {}
```

On va vérifier les changements à l'aide de : 

```bash
helm template <nom_chart> .
```

Ici, on verra que **allow-Testing** est bien initialisé à "false". Maintenant, si on change la valeur de **env** en **staging** dans le fichier **values.yaml**, celui-ci sera bien égale à **true**.

# Explication à un enfant de 8 ans

Lorsque vous vous réveiller le matin et vous vous demandez comment vous habillez. Imaginons que vous prenez un pantalon jaune. Si vous prenez un pantalon, vous n'allez sûrement pas mettre du vert ou du rouge en haut, mais sûrement quelque chose de passe-partout comme du noir ou du blanc. Ici, c'est la même chose avec les boucles **if/else**.

```yaml
{{if pantalon eq "jaune"}}
t-shirt=noir
{{else}}
t-shirt=blanc
{{end}}
```