# Le concept

Helm fournit à disposition lors de la création d'une chart Helm un fichier **values.yaml**, nous allons voir comment l'utiliser.

# Ce que j'ai compris

Lors de la génération d'une chart Helm à l'aide de la commande :

```bash
helm create <nom_chart>
```

Helm fournit une arborescence de dossier/fichier comme on a pu le voir précédemment :

```tree
<nom_chart>
├───template
│   ├─── values.yaml
│   ├─── chart.yaml
│   ├─── secret.yaml
│   ├─── configmap.yaml
└───charts
```

Penchons nous maintenant sur le fichier **values.yaml**. Comme nous avons vu précédemment, pour utiliser la syntaxe Helm et une valeur déjà prédéfinis dans un autre fichier, il nous suffira d'utiliser les **{{}}**.
Ce fichier yaml va nous permettre de stocker toutes les variables qui vont pouvoir être utilisé dans notre dossier **template**. Ces valeurs, qui sont générées dans ce fichier, sont des suggestions, libre à nous de les modifier quand on le souhaite.

Ex du fichier **values.yaml** générés :

```yaml
# Default values for first-chart.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

replicaCount: 1

image:
  repository: nginx
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: ""

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: ClusterIP
  port: 80

ingress:
  enabled: false
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: chart-example.local
      paths: []
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

nodeSelector: {}

tolerations: []

affinity: {}
```

Maintenant, nous voulons ajouter des variables d'environnement à notre chart Helm. Pour cela, nous allons crée un fichier **staging-values.yaml** dans le répertoire templates avec le code ci-dessous :
```yaml
staging:
  sample-key: sample-12345
```

Nous allons maintenant copier ce code dans notre fichier **values.yaml** :

```yaml
# Default values for first-chart.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

staging:
  sample-key: sample-12345

replicaCount: 1

image:
  repository: nginx
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: ""

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: ClusterIP
  port: 80

ingress:
  enabled: false
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: chart-example.local
      paths: []
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

nodeSelector: {}

tolerations: []

affinity: {}
```

On va vouloir accéder aux données que l'on vient d'écrire dans le fichier **values.yaml**.
Pour cela, on utilisera les brackets de la manière suivante :
**{{ .Values.staging.sample-key }}**
.Values -> Nom du fichier
.staging -> pointer sur la paire de clef
.sample-key -> pointer sur la valeur de sample-key

# Explication à un enfant de 8 ans

Prenons l'exemple d'un dessin à réaliser. Plutôt que votre professeur projettent au tableau, et que vous ayez à recopier l'intégralité du dessin, souvent le dessin sera photocopier pour vous faire gagner du temps. Ici, on retrouve les mêmes principes, on va déclarer nos variables, puis les utiliser avec les **{{}}** pour gagner un maximum de temps.