# Le concept

Ce module va mettre en avant comme rendre des valeurs dynamiques du fichier ConfigMap à l'aide de template Helm.

# Ce que j'ai compris

Plutôt que devoir mettre à la main la version de notre application ou encore le nom de l'application. Nous allons pouvoir utiliser les template Helm afin de répondre à nos besoins de la manière la plus simple possible. Dans un premier temps, rendons nous dans le dossier **templates** de notre chart Helm. Le répertoire donne quelque chose comme ci-dessous:
```tree
templates
├───Chart.yaml
|───secret.yaml
├───Values.yaml
└───ConfigMap.yaml
```
Nous allons modifier notre fichier ConfigMap.yaml de la manière suivante :

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: first-chart-configmap
data:
  port: "8080"
```

Et le fichier Chart.yaml :
```yaml
apiVersion: v2
name: first-chart
description: A Helm chart for Kubernetes

# A chart can be either an 'application' or a 'library' chart.
#
# Application charts are a collection of templates that can be packaged into versioned archives
# to be deployed.
#
# Library charts provide useful utilities or functions for the chart developer. They're included as
# a dependency of application charts to inject those utilities and functions into the rendering
# pipeline. Library charts do not define any templates and therefore cannot be deployed.
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.1.0

# This is the version number of the application being deployed. This version number should be
# incremented each time you make changes to the application. Versions are not expected to
# follow Semantic Versioning. They should reflect the version the application is using.
appVersion: 1.16.0

```

Maintenant, le but étant de rendre les valeurs dynamiques en utilisant nos templates Helm. Pour cela, on peut pointer sur une valeur d'un attribut Helm à l'aide de **{{}}**.
On va chercher à rendre le nom de notre application de façon dynamique en ajoutant à la suite de **first-chart-configmap** le numéro de version de notre application. Pour cela, on va récupérer la valeur de la version de l'application de notre fichier **Chart.yaml**.
On utilisera les **{{}}** de la manière suivante :

```yaml
metadata:
  name: first-chart-configmap-{{.Chart.version}}
```

On va remplacer ceci dans notre fichier configMap.yaml :

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: first-chart-configmap-{{.Chart.Version}}
data:
  port: "8080"
  allowTesting: "true"
```

Faisons le test grandeur nature. Mettons à jour notre chart Helm :
```bash
helm upgrade first-chart .
```

Vérifions que le nom de notre chart a bien été mis à jour en listant les **ConfigMap** grâce à kubectl:

```bash
kubectl get cm .
```

On pourra voir que le nom de notre application sera la suivante : 
- first-chart-configmap-0.1.0

Imaginons maintenant que nous avons fait quelques modifications sur notre chart Helm et que nous souhaitons mettre à jour notre application de 0.1.0 à 0.1.1. On modifiera le fichier Chart.yaml de la manière suivante :

```yaml
apiVersion: v2
name: first-chart
description: A Helm chart for Kubernetes

# A chart can be either an 'application' or a 'library' chart.
#
# Application charts are a collection of templates that can be packaged into versioned archives
# to be deployed.
#
# Library charts provide useful utilities or functions for the chart developer. They're included as
# a dependency of application charts to inject those utilities and functions into the rendering
# pipeline. Library charts do not define any templates and therefore cannot be deployed.
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.1.1

# This is the version number of the application being deployed. This version number should be
# incremented each time you make changes to the application. Versions are not expected to
# follow Semantic Versioning. They should reflect the version the application is using.
appVersion: 1.16.0
```

On met à jour notre chart Helm :

```bash
helm upgrade first-chart .
```

On vérifie que le nom de l'application soit bien **first-chart-configmap-0.1.1** à l'aide de kubectl :

```bash
kubectl get cm .
```

# Explication à un enfant de 8 ans

Imaginons qu'une action de la vie de tous les jours peut-être automatisés de sorte à vous rendre la vie un peu plus simple. On pourra prendre par exemple le **Ctrl+C** qui nous permet de copier un texte sur votre ordinateur et **Ctrl+V** qui va vous permettre de le coller. Plutôt que de vous embêter à écrire le texte, vous aurez besoin de seulement le copier et le coller, l'utilité des **{{}}** va nous permettre de rendre nos valeurs dynamiques, notre application et notre code plus propres tout en gagnant en efficacité et en temps.