# Le concept

Bien qu'il est possible d'utiliser le Hub Helm pour déployer une application tierce sur notre cluster Kubernetes. Ce module traitera la possibilité de créer sa propre chart Helm et donc son application.

Le Hub Helm met à disposition tout un tas de "third-party software" pouvant être déployer sur votre cluster Kubernetes. En effet, si vous avez besoin d'analyser des logs au sein de votre cluster Kubernetes, l'application "kube-metrics-service" peut vous rendre service, mais de temps en temps les entreprises auront besoin de crée des applications pour leur propre besoin. Helm fournit la possibilité de le faire en utilisant la commande:
```bash
helm create <nom_application>
```
Une fois celle-ci crée, en se plaçant dans le dossier de votre application, vous y trouverez deux dossiers mais aussi deux fichiers utile à la création de notre application. Nous verrons dans un prochain module comment les utiliser.
```
<nom_application>
├───charts
└───template
├───Chart.yaml
└───values.yaml
```

# Explication à un enfant de 8 ans

Souvent, lorsque vous êtes enfant que ce soit en maternelle ou en primaire. Votre enseignant peut-être amené à vous demander de colorier une figure suivant le résultat d'opérations mathématiques. En effet, il a pu trouver cet exemple sur Internet avant de vous le donner à le faire. On peut donc faire l'analogie avec le Hub Helm. Mais si cela ne lui convient pas, il pourra créer soit même le dessin que vous serait amené à faire avec des fonctionnalités supplémentaires ou une complexité plus élevé, c'est le même principe avec la commande **helm create**, nous allons créer notre propre application avec des besoins qui nous sont propres.