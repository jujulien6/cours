# Le concept

Dans ce module, nous verrons comment protéger des informations confidentielles via Helm pour notre cluster Kubernetes.

# Ce que j'ai compris

Kubernetes secrets stocke certaines informations confidentielles comme des clefs SSH ou encore des mots de passe. Si vous stocke ses informations dans vos arborescences de fichier, ces informations devront être encodées en base 64.

Dans un premier temps, sous le dossier templates, nous allons créer un fichier secret.yaml qui contient les informations suivantes:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: first-secret
type: Opaque
data:
  username:
  password:
 
---
# To convert to base64
username: 'admin'
password: '4w572$9sns1&!'
```

Le but étant ensuite, d'encoder ces informations sensibles en base 64. 

Pour cela, ouvrez un terminal de commande et copier au préalable la valeur de l'username.
```bash
echo -n "<valeur username ex:admin> | base 64
```
La sortie du terminale vous donnera un résultat encoder en base 64, vous la remplacerez par la valeur de l'username.
Vous ferez de même pour le mot de passe :
```bash
echo -n "<valeur mot de passe ex:4w572$9sns1&!> | base 64
```
Vous n'oublierez pas de le remplacer dans le fichier secret.

On obtiendra donc un nouveau fichier, par ex:
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: first-secret
type: Opaque
  username: YWRtaW4=
  password: NHc1NzIkOXNuczEmIQ==
```

Maintenant, vérifions que nos changements ont bien été pris en compte dans notre chart Helm avec la commande :
```bash
helm template first-chart .
```

On verra apparaître notre username et password avec les nouvelles modifications et donc encodées en base64.
Maintenant, nous allons vouloir appliquer ces changements à notre chart-Helm.
```bash
helm upgrade first-chart .
```

Une fois notre chart Helm déployées, on va vérifier que nos informations sont bien encodées et pas destinées à être utilisé par des hacker.
Dans un premier temps, on va afficher la liste des secrets dans notre chart Helm et dans le namespace par défaut : 
```bash
kubectl get secrets
```

Une fois trouvé, le secret que l'on vient de crée, nous allons vouloir afficher plus d'informations à son propos. Dans notre cas,il se nomme : **first-secrets**.
```bash
kubectl describe first-secrets
```
Ce qui est très intéressant ici, c'est qu'on verra l'username et le password affiché mais sous forme de bytes car ils ont été préalablement encodés et donc protégés. Si on n'avait pas fait ça, on aurait vu le mot de passe et le compte utilisateur en clair.

# Explication à un enfant de 8 ans

Imaginons que vous ayez un journal intime rangé dans votre chambre. N'importe qui pourrait le lire. Ici, avec Kubernetes secrets, nous allons protéger des informations confidentielles. Faisons l'analogie maintenant avec votre journal intime, si vous voulez le protéger, vous allez par exemple devoir mettre en place un cadenas, ou des mots de codes pour pouvoir le lire. Ici, c'est le même principe qu'on retrouve avec la base 64 qui va protéger nos informations importantes et confidentielles de notre chart Helm.