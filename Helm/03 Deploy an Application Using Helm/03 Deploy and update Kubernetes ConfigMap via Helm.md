# Le concept

Le but de ce module est de comprendre à quoi sert ConfigMap, le déployer et mettre à jour sa version suivant nos besoins.


# Ce que j'ai compris

"ConfigMap" est un objet Kubernetes utilisé pour stocker des informations peu confidentielles
comme le numéro de port ou des variables d'environnement.

Dans un premiers temps, nous allons supprimer le dossier template de notre chart Helm crée précédemment que l'on nommera **first-chart**. Ensuite, le recrée puis insérer un fichier nommer **cm.yaml**.
Dedans, on insérera le code ci-dessous:
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: first-chart-configmap
data:
  port: "8080"
```

Une fois ceci fait, le but va être de déployer notre chart Helm sur notre cluster Kubernetes.
```bash
helm install firstchart .
```

On vérifie l'installation à l'aide de la commande suivante :
```bash
kubectl describe cm first-chart-configmap
```

Une fois que tout est bon, on s'aperçoit qu'on veut rajouter **allowTesting: "True"** à la fin de notre fichier **cm.yaml**. On change le fichier comme ci-dessous:
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: first-chart-configmap
data:
  port: "8080"
  allowTesting: "true"
```

Maintenant, on va vérifier la modification de notre code, puis mettre à jour notre application et vérifier que tout soit ok.
```bash
helm template first-chart.
helm upgrade first-chart .
kubectl describe cm first-chart-configmap
```


# Explication à un enfant de 8 ans

Le fichier **cm.yaml** définit notre application que ce soit au niveau du nom, des informations, etc. Ce fichier peut évoluer au fil du temps selon nos besoins. La commande **helm upgrade** permet de mettre à jour notre application à travers ce fichier **cm.yaml**.