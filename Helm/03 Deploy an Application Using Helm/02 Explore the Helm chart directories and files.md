# Le concept

Explorer les différents fichiers et dossiers lors de la création d'une chart Helm.


# Ce que j'ai compris

On a pu voir dans le module précédent qu'on pouvait créer notre propre chart Helm pour répondre à des besoins uniques. On rappelle l'arborescence des dossiers lors de la création de notre chart Helm.
```
<nom_application>
├───Charts
└───templates
|    └───_helpers.tpl
|    └───deployment.yaml
|    └───hpa.yaml
|    └───ingress.yaml
|    └───service.yaml
|    └───serviceaccount.yaml
|    └───NOTES.txt
├───Chart.yaml
│   
└───values.yaml
```

Dans un premier temps, le fichier **chart.yaml** va répertorier tout un tas d'informations sur notre chart yaml. En effet, lorsque ce fichier est crée, quelques valeurs sont déjà préremplies. Ce fichier contient les informations les plus importantes comme :
- Nom de l'application
- Api version
- Description
- Application version

Ensuite, le fichier **values.yaml** va nous permettre de configurer nos conteneurs. Dessus, on pourra retrouver par exemple le nombre de pods que l'on veut répliquer, le port sur lesquel l'application est disponible, l'image utilisée pour les conteneurs, la définition de nos services. Ce sont toutes les variables qui vont définir notre application.

Puis, le dossier **charts**, lorsque celui-ci est initialisé, il sera vide. Le but de ce dossier est de stocker des charts Helm intermédiaire qui seront utilisé par la **chart.yaml** principale pour construire des applications.

Enfin, pour finir le dossier **templates**. Celui-ci comprend beaucoup plus d'informations utilisé pour construire notre chart Helm. En premier lieu, le fichier NOTES.txt qui affichera plusieurs informations lorsque un utilisateur aura installé ou télécharger votre chart. Dans ce dossier la, vous obtiendrez un aperçu de ce qu'est un template de la syntaxe Helm. La syntaxe est la suivante : **{{ }}**. En fait, les paramètres qui ont été passés par valeurs dans la **chart.yaml** ou **values.yaml** vont être récupérés pour être transformés en objet Kubernetes. Les autres fichiers vont permettre par exemple à créer :
- notre déploiement (deployment.yaml)
- nos pods
- nos service (service.yaml)
- pouvoir accéder à notre application sur internet (ingress.yml)

# Explication à un enfant de 8 ans

Reprenons l'exemple de la création d'un jeu vidéo sur le PlayStore et faisons l'analogie avec les différents fichiers dossiers de notre chart Helm.

Le fichier chart.yaml peut-être assimilée aux informations que vous pouvez lire en bas de votre application lorsque vous souhaitez la télécharger sur le playstore.

Le fichier values.yaml va permettre à l'application de s'éxécuter sur tels ou tel console. Si vous jouez sur Switch ou sur PS5, le modèle de déploiement de l'application ne sera pas le même.

Le dossier charts peut-être assimilée aux conditions générales d'utilisation de notre application ou des informations de l'éditeur.

Le dossier template lui est ce qui va permettre de créer votre application pour que vous puissiez y jouer.