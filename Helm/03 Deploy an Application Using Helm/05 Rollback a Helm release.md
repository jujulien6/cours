# Le concept

Ce module va traiter de la possibilité de revenir en arrière sur une ancienne version de notre Chart Helm.

# Ce que j'ai compris

Le déploiement d'une nouvelle Chart Helm peut entraîner certains problèmes en production. Ex : une fonctionnalité qui ne marche plus, un menu moins intuitif, des problèmes de sécurité. Helm met à disposition la possibilité de faire un retour en arrière.

Premièrement, avec Helm history, nous aurons un visuel de l'ensemble des déploiements effectuées au sein de notre cluster Kubernetes, avec comme informations :
- l'app version
- la date de mise en service
- une description
- le status
- le nom de notre chart
- revision ( numéro de déploiement, ex: premier déploiement = numéro 1, etc.)

Pour obtenir ces informations :
```bash
helm history <nom de votre chart Helm (first-chart)>
```

Ensuite, nous allons vouloir faire un rollback de notre déploiement. Pour cela, vous avez deux choix :
- Si vous voulez revenir à la version juste avant :
```bash
helm rollback <nom de votre chart Helm (first-chart)>
```

- Si vous voulez revenir à une version beaucoup plus ancienne :
```bash
helm rollback <nom de votre chart Helm (first-chart)> <numéro de version (2)>
```

Une fois votre rollback effectué, vous pouvez toujours vérifier à l'aide la commande **helm history** :
```bash
helm history <nom de votre chart Helm (first-chart)>
```

# Explication à un enfant de 8 ans

Reprenons l'exemple du coloriage d'un dessin suivant des opérations mathématiques.
En effet, lorsque vous avez fini, votre professeur vous demande souvent de vous relire, le fait de vous relire peut-être associé à la commande **helm history** qui elle va afficher l'historique de vos déploiements.
Mais imaginons, dans un notre cas que vous ayez commis une erreur, vous aller soit prendre votre gomme pour effacer cette erreur, et recommencer pour que celle-ci soit bonne. Dans ce cas-là, on utilisera **helm rollback**.