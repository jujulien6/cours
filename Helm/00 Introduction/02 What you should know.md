# Le concept

Vous devez avoir des bonnes bases sur :

- Kubernetes
- Avoir accès à un cluster Kubernetes
- Avoir des bonnes bases sur les commandes linux
- Avoir besoin d'un éditeur de texte pour Helm

# Ce que j'ai compris

- Savoir utiliser Kubernetes, par exemple le fait de pouvoir décrire un pods, un service, un déploiement.
  - Un pods peut contenir plusieurs ou un conteneurs
  - Un service va définir nos pods et la manière d'y accéder
  - Un déploiement va être la manière dont nos pods sont déployées, la mise à jour, la suppression, revenir à un état 
    antérieur 
- D'avoir accès à un cluster Kubernetes, on peut tester l'accès à un cluster Kubernetes à l'aide de la commande "kubectl get" and "kubectl describe"
- Des bonnes bases en linux : création/modification/suppression de dossier ou fichier en ligne de commande
- D'un éditeur de texte pour Helm ( Visual studio code, vim, nano)

# Expliquer à un enfant de 8 ans

Pour pouvoir déployer notre applications, il nous faut de solide bases. Reprenons l'exemple d'avant avec le coloriage suivant des opérations mathématiques, pour pouvoir obtenir notre figure à la fin. L'enfant aura besoin de plusieurs choses:
- des crayons de couleurs
- connaître ses couleurs
- connaître ses opérations mathématiques (soustraction, division, multiplication, etc.)