# Le concept

Helm est un gestionnaire de package pour kubernetes pour faciliter le déploiement et la mise à jour de vos applications

# Ce que j'ai compris

La gestion de plusieurs fichiers yaml pour le déploiement de son application peut-être frustrante et assez longue, c'est dans ce sens la qu'a été crée Helm pour faciliter la mise à jour, le maintien de version mais aussi le retour en arrière du déploiement de vos applications.

# Expliquer à un enfant de 8 ans

Cela pourrait-être associé à un dessin ou l'on doit colorier les couleurs suivant des résultats mathématiques, en effet celui-ci serait déjà remplies, mais on pourrait revenir en arrière si il y a eu des erreurs dans le remplissage