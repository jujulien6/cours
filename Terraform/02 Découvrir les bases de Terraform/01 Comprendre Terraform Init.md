# Le concept

Comprendre comment fonctionne Terraform init.

# Ce que j'ai compris

Votre infrastructure sera décrite à l'aide d'un ou plusieurs fichiers Terraform d'extension **.tf**.
La première étape va être l'analyse du code de vos fichiers pour que Terraform puisse analyser quel API appelé pour pouvoir générer votre infrastructure sur le provider choisi. Une fois, ceci fait, Terraform va stocker l'état de l'infrastructure dans un fichier local sur le dossier de travail appelé **Terraform.tfstate** (format json).
Cette étape **Init** va aussi permettre d'installer tout ce qui est nécessaires pour que l'infrastructure puisse être initialisé comme les plugins des providers, etc.

# Explication à un enfant de 8 ans

Terraform init va vous permettre d'avoir une base solide pour commencer à déployer votre infrastructure (plugins, provider, etc.), sans celle-ci, vous ne pourrez pas travailler dans de bonnes conditions. Elle sauvegarde aussi l'état réel de votre infrastructure, c'est un peu comme votre carnet de liaison, il assure votre suivi toute l'année. On peut aussi comparer à ça à la préparation d'un match de foot, si vous n'avez pas vos crampons, votre maillot, votre short, le match n'est pas préparé dans de bonnes conditions, pareilles si vous arrivez en retard ou sans échauffement.