# Le concept

Appliquer Terraform apply.

# Ce que j'ai compris

Après avoir créé notre plan d'exécution, nous allons maintenant vouloir mettre en œuvre les changements nécessaires pour obtenir l'état souhaité de notre infrastructure. Terraform apply va interpréter par défaut le répertoire courant, ou exécuté l'ensemble des actions générés par le plan d'exécution de la commande **Terraform plan**. Terraform apply va appliquer l'ensemble des ressources souhaités par l'utilisateur, comme c'est une commande en mode interactif, il vous faudra écrire yes pour appliquer les changements. Si vous effectuez l'apply par rapport à un plan, on ne vous demandera pas de valider les changements, cela se fera automatiquement.

# Explication à un enfant de 8 ans

Terraform apply va vous permettre de déployer l'infrastructure. C'est l'étape final de votre déploiement. Vous pouvez en faire autant que vous voulez suivant l'état désiré. On peut comparer ça au rendu de votre copie lors d'un examen.