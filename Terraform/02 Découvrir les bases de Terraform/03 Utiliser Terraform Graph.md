# Le concept

Savoir comment utiliser Terraform Graph.

# Ce que j'ai compris

Avant d'appliquer notre plan, Terraform fournit à disposition une option visuelle pour de notre configuration. Avec **Terraform graph --help**, on en apprendra plus sur la commande.
Il y a deux options principales avec cette commande :
1. **draw-cycles**, permet de faire remonter des erreurs de types relations
2. **type**, permet de définir sur quel graphe on va générer (plan, apply, destroy)

Lorsqu'on utilise la commande terraform graph, celui-ci affiche une sortie standard dans un format appelé **Dot**, c'est ce format qu'on exportera vers d'autres applications comme [Graphviz] (https://graphviz.org/download/) par exemple. Graphviz fournit un IDE [online] (https://dreampuf.github.io/GraphvizOnline), il ne vous restera plus qu'à copier votre sortie standard pour obtenir une vue graphique.

# Explication à un enfant de 8 ans

Terraform graph va fournir une représentation graphique de votre déploiement, il peut vous aider dans la configuration ou déploiement de votre infrastructure, comme lorsque la prof schématise une explication au table (par exemple la soustraction avec des billes).