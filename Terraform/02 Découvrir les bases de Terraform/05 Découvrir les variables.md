# Le concept

Utiliser les variables pour configurer vos fichiers Terraform.

# Ce que j'ai compris

On peut avoir plusieurs façons de définir des variables dans Terraform, elles sont définies dans des blocs.

1. Variables de type chaîne de caractères

```terraform
variable "image_id" {
  type = string
  default = "ami-xxx"
}
```

Ici, on a une variable "image_id" dont on définit le nom et l'image.

2. Variables de type liste de chaîne de caractère

```terraform
variable "availability_zones" {
  type = list(string)
  default = ["eu-west-1a","eu-west-1b","eu-west-1c"]
}
```

Ici, on va définir un tableau de valeur pour les régions AWS. On sélectionnera la région souhaité ensuite.

3. Variable de type objet ou liste de clé=valeur

```terraform
variable "tags_list" {
  type = map
  default = {
    Name = "Web Server"
    Environment = "Development"
  }
}
```
Ici, on va définir la liste des tags et chaque tag a une clé et une valeur.

Enfin, on définira une ressource avec nos variables :

```terraform
ressource "aws_instance" {
  instance_type = "t2.micro"
  ami = var.image_id
  tags = var.tags_list
  availability_zone = var.availability_zones[0]
}
```

Pour utiliser les variables, vous devrez utiliser le préfixe **var** suivit de la variable que vous voulez utiliser. Ex : Si vous voulez tags_list -> var.tags_list

4. Variables de type data

Une fois l'infrastructure déployé, vous pouvez extraire les données de celle-ci. Le fichier `data.tf`: 

```terraform
data "aws_ami" "image_web" {
  owners = ["self"]
  filter {
    name = "tag:Tested"
    values = ["true"]
  }
  most_recent = true
}

ressource "aws_instance" "instance2" {
  instance_type = "t2.micro"
  ami = data.aws_ami.image_web.iud
  tags = {
    Name = "Web Server"
  }
}
```

Ici, on va filtrer dans un premier temps pour récupérer les `ami` déployés et filtrer par la notion de tags dont le tag `tested` a une valeur true. En récupérant l'id de l'image, on va pouvoir déployer une nouvelle machine avec la même ami que celle existant sur notre infrastructure et comme tag "Web Server".

5. Variable locale

On peut aussi définir des variables, ces variables seront uniques dans un `même module`. Exemple avec une image ami que l'on veut utiliser plusieurs fois pour déployer nos instances.

```terraform
locals{
  web_ami = "ami-0cfed801f3165b72c"
}

ressource "aws_instance" "instance3" {
  instance_type = "t2.micro"
  ami = local.web_ami
  tags = {
    Name = "Web Server"
  }
}

ressource "aws_instance" "instance4" {
  instance_type = "t2.micro"
  ami = local.web_ami
  tags = {
    Name = "Web Server"
  }
}
```

6. Variable pour appliquer notre code Terraform

On peut aussi très bien passer en argument les variables au moment du déploiement de notre code Terraform.

```bash
terraform apply -var image_id=""
```

# Explication à un enfant de 8 ans

Une variable associe une valeur à un objet, l'objet garde en mémoire sa valeur mais peut évoluer au cours du temps. Prenons l'exemple d'une roue de voiture, elle peut être défini par sa couleur de roue, ses jantes, le modèle, le nombre de pouces, etc. Ici, c'est le même principe avec Terraform, nous allons utilisé des variables pour pouvoir déclarer nos objets par la suite.