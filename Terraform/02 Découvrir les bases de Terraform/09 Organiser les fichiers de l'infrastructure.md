# Le concept

Dans ce module, on verra comment organiser les fichiers de notre infrastructure.

# Ce que j'ai compris

Un des best-practice pour l'organisation de notre répertoire de travail pour Terraform est le suivant :

```tree
.
├───main.tf
├───variables.tf
├───outputs.tf
└───modules
    ├───aws-ec2-website
    │   ├───LICENSE
    │   ├───README.md
    │   ├───main.tf
    │   ├───outputs.tf
    │   ├───variables.tf
    ├───aws-vpc-website
    │   ├───LICENSE
    │   ├───README.md
    │   ├───main.tf
    │   ├───outputs.tf
    │   ├───variables.tf

```

Le dossier est réparti de la manière suivante :

- Au niveau de la racine :
     - main.tf pour l'exécution de notre code Terraform
     - variables.tf qui va contenir toutes les variables du module principal (racine) et des sous 
       modules
     - outputs.tf qui va contenir les sorties standards qu'on va effectuer à partir de ce module
- Au niveau du sous-module module, on aura la même arborescence avec comme fichier supplémentaire LICENSE et README.md pour la documentation. Dans les sous-dossiers, on aura un sous-dossier pour chaque service utilisé dans le Cloud, ici AWS. Ici, on en a deux :

     - aws-vpc-website
     - aws-ec2-website

# Explication à un enfant de 8 ans

Pour pouvoir travailler dans les meilleures conditions possibles et avoir un code propre pour vérification en cas d'erreur, il faut que notre dossier de travail soit organisé et suit les best-practice. Pour cela, on va séparer nos modules de nos fichiers racine. Ainsi chaque sous-module qui comprend un service dans le cloud (vpc, ec2), par exemple la création de machines virtuelles ou autres va contenir les fichiers suivants :
- main.tf (exécution de notre code)
- variables.tf (variables que l'on définira)
- outputs.tf (sortie standard de notre code)
- README.md (documentation)

Le dossier racine, lui va contenir les mêmes fichiers, mais eux serviront pour l'exécution final de notre code Terraform.