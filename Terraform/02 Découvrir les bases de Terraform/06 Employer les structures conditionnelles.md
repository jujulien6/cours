# Le concept

Utiliser les structures conditionnelles avec Terraform.

# Ce que j'ai compris

Dans ce module, on utilisera la structure conditionnelle `if` pour gérer nos conditions. Comment fonctionne la condition if sous Terraform :

`condition ? valeur_si_vrai : valeur_si_faux`

Les opérateurs sont les suivants :

- `==` égal à
- `!=` différent de 
- `<=` inférieur ou égal à
- `<`  inférieur
- `>=` supérieur ou égal à
- `>`  supérieur
- `||` ou
- `&&` et
- `!`  not

Dans l'exemple suivant, on va créer une instance ec2 appelé "web instance". On va initialiser count à la valeur de la variable create_instance, si elle est égale à 1, on crée notre instance sinon on ne l'a crée pas. Pour notre "subnet_id", si la variable environnement est égal à production, on initialisera notre subnet avec celui de production, sinon, ce sera production. On pourra modifier notre fichier suivant nos besoins.

```terraform
resource "aws_instance" "web_instance" {
  count = var.create_instance ? 1 : 0
  subnet_id = vat.environment == "production" ? aws_subnet.prod_subnet.id : aws_subnet.dev_subnet.id
  instance_type = "t2.micro"
  ami = "ami-0cfed801f3165b72c"
  tags = {
    Name = "Web Server"
  }
}
```

En déployant notre infrastructure, nous pourrons aussi initialiser nos variables. Ex : 

```terraform
terraform apply -var environment = "dev"
```

En utilisant ces variables-là, on créera une instance web_instance avec comme subnet celui de développement.

# Explication à un enfant de 8 ans

Les structures conditionnelle et en particulier if vont être utilisé pour tester des conditions et ainsi l'appliquer à notre infrastructure suivant des besoins précis. On peut faire l'analogie avec votre emploi du temps, par exemple si vous avez Mathématiques le lendemain, vous allez prendre votre manuel sinon il restera chez vous.