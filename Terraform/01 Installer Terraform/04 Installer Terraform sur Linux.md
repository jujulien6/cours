# Le concept

Installer Terraform sur Linux.

# Ce que j'ai compris

On va installer Terraform sur notre machine linux. Sur le site de [Terraform](https://developer.hashicorp.com/terraform/downloads?product_intent=terraform) sont disponibles les packages d'installation ou la manière de l'installer. Pour linux, vous avez deux manières d'installer :

1. Télécharger le package suivant l'OS utilisé
2. Sur un terminal de commande :

```bash
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install terraform
```

Ensuite, pour vérifier, tapez **Terraform** dans un terminal.

# Explication à un enfant de 8 ans

Pour installer Terraform sur un ordinateur Linux, tu dois suivre ces étapes :

1. Télécharge l'application Terraform sur Internet. C'est comme un jeu sur ton ordinateur.
2. Ouvre l'application Terraform une fois qu'elle est téléchargée. C'est comme si tu voulais jouer à ce jeu.
3. Suis les instructions à l'écran pour installer Terraform sur ton ordinateur. C'est comme si tu devais suivre les instructions d'un puzzle pour savoir comment le monter.
4. Une fois que Terraform est installé, tu peux construire ton infrastructure. C'est comme si tu devais construire un puzzle.