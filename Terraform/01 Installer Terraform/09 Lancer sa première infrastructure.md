# Le concept

On utilisera Terraform pour déployer notre première infrastructure dans AWS.

# Ce que j'ai compris

Nous allons créer une instance Ec2 avec Terraform.
Créer un dossier de travail. Ex : **mkdir 01/09**, puis crée un fichier **main.tf**.

```terraform
provider "aws" {
  region = "eu-west-3"
}

resource "aws_instance" "instance1"{
  ami = "ami-055fc45692cb976ff"
  instance_type = "t2.micro"
}
```

Dans ce fichier **main.tf**, on définit plusieurs blocs pour nos objets. Le premier définira le provider ainsi que notre région ("eu-west-3" = Paris), le deuxième bloc lui définira une instance Ec2 de type "t2.micro" avec comme image (ami) un serveur nginx web.

Pour initialiser notre infrastructure :

```bash
terraform init
```

Ensuite, on va définir un plan, on va planifier notre installation. Une fois le plan lancé, celui-ci récapitulera les informations que l'on veut déployer. 

```bash
terraform plan
```
Pour pouvoir déployer votre infrastructure :
```bash
terraform apply 
```
On pourra vérifier sur la console AWS que notre instance soit bien crée.

# Explication à un enfant de 8 ans

On a vu précédemment comment crée un utilisateur AWS pour pouvoir crée notre propre infrastructure.

Ici, le provider définira l'endroit où vous allez travailler. Par exemple, si vous allez réviser chez vous, ou dans une bibliothèque.

La ressource définira ce que vous voudrez, ici, on veut une machine Linux. Vous par exemple, vous allez peut-être travailler votre poésie ou les mathématiques.

Ensuite, pour pouvoir créer notre infrastructure, prenons l'exemple d'un exercice.

1. Terraform init : vous allez prendre connaissance de la consigne.
2. Terraform plan : vous allez commencer à réfléchir/répondre à la consigne.
3. Terraform apply : vous allez rendre votre devoir.