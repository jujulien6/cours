# Le concept

Dans ce module, on en apprendra plus sur Terraform.

# Ce que j'ai compris

Terraform est un outil de déploiement d'infrastructure. Il est utilisé dans l'IaC. Il peut être utilisé pour le déploiement d'infrastructure réseau, machines virtuelles, machines physiques ou encore dans le Cloud.

On pourra collaborer et partager nos informations avec les différents fichiers de configuration, sachant que Terraform est déclarative, cela est très utile si l'on veut créer des nouvelles ressources, les supprimer ou les modifier.

Avec Terraform, on va pouvoir planifier et prévoir les changements. Terraform nous offre la possibilité d'utiliser des plans pour tester notre configuration, mais aussi appliquer des changements à une infrastructure déjà existante. On va aussi pouvoir utiliser des modules pour créer notre infrastructure et pouvoir les reproduire à souhait, et surtout une dépendance au niveau des ressources. Par exemple, si on déploie une machine Ec2 Linux dans AWS, nous allons souvent la déployer avec un VPC pour le réseau.

Avec Terraform, nous allons pouvoir travailler en équipe et utiliser les outils de collaboration comme Git pour construire notre infrastructure. Lien d'installation de [Git](https://git-scm.com/downloads).

# Explication à un enfant de 8 ans

On a vu précédemment que l'IaC offre la possibilité de déployer des infrastructures. Terraform est un outil open-source développé pour faire de l'infrastructure as a code. Terraform permet de créer des nouvelles ressources (machines), les supprimer ou encore les modifier. Le travail en collaboration avec Git est très utile, en effet, une personne pourra par exemple déployer les machines, l'autre la partie réseau, l'autre s'occuper du nommage de nos machines, etc.