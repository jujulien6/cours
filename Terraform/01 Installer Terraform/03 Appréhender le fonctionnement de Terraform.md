# Le concept

Après avoir vu en quoi consiste Terraform, on en apprendra plus sur la façon de fonctionner de Terraform.

# Ce que j'ai compris

Terraform fonctionne avec plusieurs fichiers d'extension **.tf**. Ce sont ces fichiers qui vont nous permettre de créer nos ressources. Lorsque vous créez une infrastructure avec Terraform, souvent, vous allez utilisé un fournisseur cloud, c'est l'API de ce fournisseur cloud (AWS, Microsoft, Google) qui va vous permettre de créer ces ressources.

Lorsque vous créer votre infrastructure, Terraform va stocker l'état de l'infrastructure avec les objets crée dans un fichier **Terraform.tfstate.**, lorsqu'on modifiera un objet de notre infrastructure, ce fichier va être comparé ave l'tat souhaité pour pouvoir modifier notre infrastructure, ensuite le fichier **tfstate** sera mis à jour. À noter qu'il est au format d'extension **json**, on pourra donc analyser et extraire ces informations.

Avec Terraform, on note plusieurs étapes pour créer notre infrastructure :
- Terraform init : initialiser le dossier courant et les modules pré requis suivant le provider
- Terraform plan : simulation de l'infrastructure souhaitée, pas d'exécution au niveau du provider
- Terraform apply: on applique le plan précédemment crée.
- Destroy : permet de détruire notre infrastructure

Terraform repose sur le langage HCL (HashiCorp Configuration Language) qui est un langage de haut niveau avec la notion de bloc qui contient des arguments pour définir nos objets. Ex :

```terraform
terraform {
  required_provider {
    aws = {
      source = "hashicorp/aws"
      version = "~>2.70"
    }
  }
}
```

# Explication à un enfant de 8 ans

Voyons Terraform et sa suite de commandes comme un exercice mathématiques :

- Terraform init : on obtient la consigne et on prépare le matériel qu'on a besoin. (trousse, cahier, feuille de brouillon)
- Terraform plan : On prépare sur la feuille de brouillon la réponse à la consigne, on vérifie que l'opération mathématique est correcte.
- Terraform apply : une fois qu'on a vérifié au préalable nos calculs mathématiques, on l'écrit sur notre feuille d'examen
- Terraform destroy : on détruit notre feuille ou par exemple, on rend notre copie.