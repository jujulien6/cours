# Le concept

Installer Terraform sur Mac.

# Ce que j'ai compris

On va installer Terraform sur notre machine Mac. Sur le site de [Terraform](https://developer.hashicorp.com/terraform/downloads?product_intent=terraform) sont disponibles les packages d'installation ou la manière de l'installer. Sur votre terminal :

1. Dézipper le dossier de téléchargement
2. Déplacer le binaire dans le répertoire **/usr/local/bin/**

```bash
sudo mv terraform /usr/local/bin
```

3. Supprimer le dossier zip
4. Pour vérifier, tapez **Terraform** dans un terminal.

# Explication à un enfant de 8 ans

Pour installer Terraform sur un ordinateur Mac, tu dois suivre ces étapes :

1. Télécharge l'application Terraform sur Internet. C'est comme un jeu sur ton ordinateur.
2. Déplacer le dossier Terraform en le dézippant. C'est comme si tu voulais créer un dossier sur ton ordinateur avec comme raccourci le nom du jeu.
3. Une fois que Terraform est installé, tu peux construire ton infrastructure. C'est comme si tu devais construire un puzzle.