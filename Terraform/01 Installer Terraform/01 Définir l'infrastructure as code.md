# Le concept

Dans ce module, on définira l'Infrastructure as a code (IaC). L'IaC concerne uniquement l'infrastructure. Je m'explique, on peut créer une base de données à partir de l'IaC mais on ne pourra pas créer des tables ou encore ajouter des utilisateurs, dans ce cas la, on pourra par exemple utiliser Ansible. Terraform est un des nombreux outils d'Infrastructure as a Code

# Ce que j'ai compris

L'infrastructure as a code est utilisé pour l'automatisation d'infrastructure, très utilisé par les profils DevOps, l'IaC nous permet de déployer des serveurs, une partie réseau, des bases de données, des sites web, etc.

L'IaC est basé sur un langage déclaratif, en effet, on va définir les objets de notre infrastructure comme les bases de données.

Ce qui est très intéressant avec l'IaC, c'est **l'idempotence**. Imaginons que vous déployer une première fois votre infrastructure, mais lors du déploiement, vous vous apercevez qu'il vous manque quelque chose, vous allez l'ajouter à votre déploiement. L'IaC ne va pas redéployer ce qui l'est déjà mais juste ajouté la partie manquante.

Plusieurs outils open-source nous permettent de faire de l'IaC :
- Terraform (Multi-Cloud)
- Vagrant (hyperviseur : VMWare)

# Explication à un enfant de 8 ans

De plus en plus, l'option d'automatiser reste la plus privilégiée en entreprise, c'est dans ce sens-là qu'est apparu IaC. Les cloud-providers vous offre la possibilité de demander des services via leur interface Web, cela peut être long et redondant. Le cloud est l'accès et stockage grâce à Internet, ces entreprises mettent à disposition des services (ordinateur, disque dur, etc.). L'IaC va permettre de créer ces services tout en automatisant nos tâches, par exemple les noms des pcs, taille disque, dur, s'il manque un PC ou non.