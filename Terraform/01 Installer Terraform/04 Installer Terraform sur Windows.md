# Le concept

Installer Terraform sur Windows.

# Ce que j'ai compris

On va installer Terraform sur notre machine Windows. Sur le site de [Terraform](https://developer.hashicorp.com/terraform/downloads?product_intent=terraform) sont disponibles les packages d'installation ou la manière de l'installer.

1. Télécharger le package pour un OS Windows
2. Dans votre arborescence Windows, extraire le fichier d'installation et créer un dossier dans votre disque C nommé **AppLocal**, copier l'extraction dans celui-ci.
3. On doit rajouter le dossier d'exécution dans la partie **Variable Path** de votre PC. Pour cela sur votre PC, aller dans votre explorateur de dossier - > PC -> Propriétés -> Systèmes avancés -> Variables d'environnement et ajouter le dossier **AppLocal** à vos variables d'environnement.
4. Pour vérifier, tapez **Terraform** dans un terminal.

# Explication à un enfant de 8 ans

Pour installer Terraform sur un ordinateur Windows, tu dois suivre ces étapes :

1. Télécharge l'application Terraform sur Internet. C'est comme un jeu sur ton ordinateur.
2. Crée un dossier pour Terraform. C'est comme si tu voulais créer un dossier sur ton ordinateur avec comme raccourci le nom du jeu.
3. Ajouter à la variable d'environnement, c'est comme si lorsque tu veux jouer, tu dois installer ta manette proprement sur le PC ou ta souris.
4. Une fois que Terraform est installé, tu peux construire ton infrastructure. C'est comme si tu devais construire un puzzle.